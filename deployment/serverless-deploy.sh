#!/bin/sh
# This shellscript is used deploy lambda function and API Gateway to Amazon with args:
# 1: id of the ftw file.
# 2: stage to deploy (dev/stg/prod)
# 3: aws-profile
# example cmd: ./serverless-deploy.sh 008 dev g1esCreds

echo "Start deploy sourcecode from repository to Amazon by shellscript...."
if [ -z "$1" ]; then
	echo "Please make sure you don't forget select stack!"
	exit;
fi

#Remove the current file serverless.yml if it existing
if [ -e "serverless.yml" ]; then
	rm -f "serverless.yml"
fi

#ln -s "globeone-es-ftw$id.yml" serverless.yml
FILE_TO_DEPLOY="globeone-es-ftw$1.yml"
echo "...file to deploy $FILE_TO_DEPLOY"
if [ -e $FILE_TO_DEPLOY ]; then
	echo "...make serverless.yml file from $FILE_TO_DEPLOY file." 
	cp $FILE_TO_DEPLOY serverless.yml
else
	echo "...file $FILE_TO_DEPLOY to deploy isn't exist!"
	exit;
fi

# Install Serverless
echo "...check and upgrade serverless framework."
npm install serverless -g

# Build serverless cmd to deploy...

echo "=====================\nServerless builder..."
SERVERLESS_CMD="serverless deploy"
# Check and set stage...
if [ -z "$2" ]; then
	SERVERLESS_CMD="$SERVERLESS_CMD -s prod"
	echo "...stage as prod."
else
	SERVERLESS_CMD="$SERVERLESS_CMD -s $2"
	echo "...stage as $2."
fi

# Check and set profile...
if [ -n "$3" ]; then
	SERVERLESS_CMD="$SERVERLESS_CMD --aws-profile $3"
	echo "...profile as $3."
fi

# Execute CMD.
$SERVERLESS_CMD
