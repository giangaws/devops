service: globeone-es-ftw006

plugins:
#  - serverless-stage-manager
#  - serverless-oauth-scopes
# Load require stages for *serverless-stage-manager*
custom:
  common:
    ${file(../common.yml)}
  stages:
    ${file(../stage.yml)}
  environment:
    ${file(../environment.yml)}
  security:
    role:
      ${file(../security/role.yml)}
    vpc:
      ${file(../security/vpc.yml)}
    cors:
      ${file(../security/cross-origin-resource-sharing.yml)}
    cognito:
      ${file(../security/cognito.yml)}

# Provider....
provider:
  name: aws
  runtime: java8
  timeout: 30
  memorySize: 512
  region: ${self:region, 'ap-southeast-1'}
  vpc:
    securityGroupIds:
      ${self:custom.security.vpc.securityGroupIds.${opt:stage, 'dev'}}
    subnetIds:
      ${self:custom.security.vpc.subnetIds.${opt:stage, 'dev'}}
  deploymentBucket:
    name: ${self:custom.common.bucket.${opt:stage, 'dev'}.name}
  role: ${self:custom.security.role.${opt:stage, 'dev'}}
  environment: ${self:custom.environment.${opt:stage, 'dev'}}

package:
  individually: true

resources:
  Resources:
    ${file(../resources/apigateway/ftw006ApiGateway.yml)}

functions:
  MobileTokenExpiredStatus:
    handler: ph.com.globe.globeone.lambda.CheckTokenExpiredHandler
    package:
      artifact: ${self:custom.common.relatedPath.sourceDirectory}postpaid/check-token-expired/target/globeone-es-mobile-check-token-expired-1.0.0-SNAPSHOT.jar
    environment:
      DAX_ENDPOINT: ${self:custom.common.DaxHost.${opt:stage, 'dev'}}
      DAX_REGION: ap-southeast-1
      APP_USER_LOGIN: ${self:custom.environment.${opt:stage, 'dev'}.DATABASE_CUSTOMER_MOBILE_TABLE}
      TOKEN_EXPIRE_DURATION: 43200
    events:
      - http:
          path: /mobile/v1/token-expired-validation
          method: post
          cors: ${self:custom.security.cors}
          authorizer: ${self:custom.security.cognito.${opt:stage, 'dev'}}

  MobilePinCodeCompare:
    handler: ph.com.globe.globeone.lambda.PinCodeHandler
    package:
      artifact: ${self:custom.common.relatedPath.sourceDirectory}postpaid/app-compare-pin-code/target/globeone-es-mobile-compare-pin-code-1.0.0-SNAPSHOT.jar
    environment:
      DAX_ENDPOINT: ${self:custom.common.DaxHost.${opt:stage, 'dev'}}
      DAX_REGION: ap-southeast-1
      APP_USER_LOGIN: ${self:custom.environment.${opt:stage, 'dev'}.DATABASE_CUSTOMER_MOBILE_TABLE}
    events:
      - http:
          path: /mobile/v1/pin/compare
          method: post
          cors: ${self:custom.security.cors}
          authorizer: ${self:custom.security.cognito.${opt:stage, 'dev'}}

  MobileForgotPinCode:
    handler: ph.com.globe.globeone.lambda.ForgotPINHandler
    package:
      artifact: ${self:custom.common.relatedPath.sourceDirectory}postpaid/forgotPIN/target/globeone-es-mobile-forgot-PIN-1.0.0-SNAPSHOT.jar
    environment:
      APP_USER_LOGIN: ${self:custom.environment.${opt:stage, 'dev'}.DATABASE_CUSTOMER_MOBILE_TABLE}
      AEM_CONFIG_TABLE_NAME: ${self:custom.environment.${opt:stage, 'dev'}.DATABASE_AEM_CONFIG_TABLE}
      SEND_EMAIL_FUNCTION_NAME: ${self:custom.common.functions.${opt:stage, 'dev'}.SendEmailNotification}
    events:
      - http:
          path: /mobile/v1/pin/forgot
          method: post
          cors: ${self:custom.security.cors}
          authorizer: ${self:custom.security.cognito.${opt:stage, 'dev'}}

  MobileLoginWith3rdParty:
    handler: ph.com.globe.globeone.lambda.Mobile3rdPartyLoginHandler
    package:
      artifact: ${self:custom.common.relatedPath.sourceDirectory}postpaid/mobile-login-with-3rd-party/target/globeone-es-mobile-login-with-3rd-party-1.0.0-SNAPSHOT.jar
    environment:
      SPARCO_ENDPOINT: ${self:custom.common.GlobeEndPoint.${opt:stage, 'dev'}.spaco}/Authentication.asmx
      DAX_ENDPOINT: ${self:custom.common.DaxHost.${opt:stage, 'dev'}}
      DAX_REGION: ap-southeast-1
      APP_USER_LOGIN: ${self:custom.environment.${opt:stage, 'dev'}.DATABASE_CUSTOMER_MOBILE_TABLE}
      GOOGLE_CLIENT_ID_ANDROID: ${self:custom.common.ThirdPartyKey.${opt:stage, 'dev'}.google-client-id-android}
      GOOGLE_CLIENT_ID_IOS: ${self:custom.common.ThirdPartyKey.${opt:stage, 'dev'}.google-client-id-ios}
      FACEBOOK_APP_SECRET: ${self:custom.common.ThirdPartyKey.${opt:stage, 'dev'}.facebook-app-secret}
    events:
      - http:
          path: /mobile/v1/login-3rd-party
          method: post
          cors: ${self:custom.security.cors}

  MobileLoginWithPin:
    handler: ph.com.globe.globeone.lambda.MobilePinLoginHandler
    package:
      artifact: ${self:custom.common.relatedPath.sourceDirectory}postpaid/mobile-login-with-pin/target/globeone-es-mobile-login-with-pin-1.0.0-SNAPSHOT.jar
    environment:
      DAX_ENDPOINT: ${self:custom.common.DaxHost.${opt:stage, 'dev'}}
      DAX_REGION: ap-southeast-1
      APP_USER_LOGIN: ${self:custom.environment.${opt:stage, 'dev'}.DATABASE_CUSTOMER_MOBILE_TABLE}
      TOKEN_EXPIRE_DURATION: 43200
    events:
      - http:
          path: /mobile/v1/login-with-pin
          method: post
          cors: ${self:custom.security.cors}
          authorizer: ${self:custom.security.cognito.${opt:stage, 'dev'}}

  MobileSetPinChangePin:
    handler: ph.com.globe.globeone.lambda.PinCodeHandler
    package:
      artifact: ${self:custom.common.relatedPath.sourceDirectory}postpaid/app-pin-code-actions/target/globeone-es-mobile-pin-code-actions-1.0.0-SNAPSHOT.jar
    environment:
      DAX_ENDPOINT: ${self:custom.common.DaxHost.${opt:stage, 'dev'}}
      DAX_REGION: ap-southeast-1
      APP_USER_LOGIN: ${self:custom.environment.${opt:stage, 'dev'}.DATABASE_CUSTOMER_MOBILE_TABLE}
      TOKEN_EXPIRE_DURATION: 43200
    events:
      - http:
          path: /mobile/v1/pin/modify
          method: post
          cors: ${self:custom.security.cors}
          authorizer: ${self:custom.security.cognito.${opt:stage, 'dev'}}

  MobileRemovePinCode:
    handler: ph.com.globe.globeone.lambda.RemovePinCodeHandler
    package:
      artifact: ${self:custom.common.relatedPath.sourceDirectory}postpaid/remove-pin-code/target/globeone-es-mobile-remove-pin-code-1.0.0-SNAPSHOT.jar
    environment:
      SPARCO_ENDPOINT: ${self:custom.common.GlobeEndPoint.${opt:stage, 'dev'}.spaco}/Authentication.asmx
      APP_USER_LOGIN: ${self:custom.environment.${opt:stage, 'dev'}.DATABASE_CUSTOMER_MOBILE_TABLE}
    events:
      - http:
          path: /mobile/v1/pin-remove
          method: get
          cors: ${self:custom.security.cors}
          authorizer: ${self:custom.security.cognito.${opt:stage, 'dev'}}

  MobileLoginWithEmailPasword:
    handler: ph.com.globe.globeone.lambda.MobileLoginHandler
    package:
      artifact: ${self:custom.common.relatedPath.sourceDirectory}postpaid/mobile-login-with-email-password/target/globeone-es-mobile-login-with-email-password-1.0.0-SNAPSHOT.jar
    environment:
      SPARCO_ENDPOINT: ${self:custom.common.GlobeEndPoint.${opt:stage, 'dev'}.spaco}/Authentication.asmx
      DAX_ENDPOINT: ${self:custom.common.DaxHost.${opt:stage, 'dev'}}
      DAX_REGION: ap-southeast-1
      APP_USER_LOGIN: ${self:custom.environment.${opt:stage, 'dev'}.DATABASE_CUSTOMER_MOBILE_TABLE}
    events:
      - http:
          path: /mobile/v1/login-with-email
          method: post
          cors: ${self:custom.security.cors}

  MobileLogOut:
    handler: ph.com.globe.globeone.lambda.MobileLogOutHandler
    package:
      artifact: ${self:custom.common.relatedPath.sourceDirectory}postpaid/mobile-log-out/target/globeone-es-mobile-log-out-1.0.0-SNAPSHOT.jar
    environment:
      APP_USER_LOGIN: ${self:custom.environment.${opt:stage, 'dev'}.DATABASE_CUSTOMER_MOBILE_TABLE}
    events:
      - http:
          path: /mobile/v1/logout
          method: post
          cors: ${self:custom.security.cors}
          authorizer: ${self:custom.security.cognito.${opt:stage, 'dev'}}

  storeEndpointArn:
    handler: ph.com.globe.globeone.lambda.StoreEndpointArnHandler
    package:
      artifact: ${self:custom.common.relatedPath.sourceDirectory}postpaid/store-endpoint-arn/target/globeone-es-postpaid-store-endpoint-arn-1.0.0-SNAPSHOT.jar
    environment:
      APP_USER_LOGIN: ${self:custom.environment.${opt:stage, 'dev'}.DATABASE_CUSTOMER_MOBILE_TABLE}
      GCM_APPLICATION_NAME: ${self:custom.common.GlobeOneSNS.${opt:stage, 'dev'}.gcm-application-name}
      GCM_APPLICATION_ARN: ${self:custom.common.GlobeOneSNS.${opt:stage, 'dev'}.gcm-application-arn}
      APNS_APPLICATION_NAME: ${self:custom.common.GlobeOneSNS.${opt:stage, 'dev'}.apns-application-name}
      APNS_APPLICATION_ARN: ${self:custom.common.GlobeOneSNS.${opt:stage, 'dev'}.apns-application-arn}
      # APNS_SANBOX_APPLICATION_NAME: PushNotificationOneGlobeDev
      # APNS_SANBOX_APPLICATION_ARN: arn:aws:sns:ap-southeast-1:495831675559:app/APNS_SANDBOX/PushNotificationOneGlobeDev
      SNS_ENDPOINT: https://sns:ap-southeast-1.amazonaws.com
      SNS_REGION: ap-southeast-1
    events:
      - http:
          path: /mobile/v1/device-endpoint-arn
          method: post
          cors: ${self:custom.security.cors}
          authorizer: ${self:custom.security.cognito.${opt:stage, 'dev'}}

  pushNotification:
    handler: ph.com.globe.globeone.lambda.NotificationSavingHandler
    package:
      artifact: ${self:custom.common.relatedPath.sourceDirectory}postpaid/send-notification/target/globeone-es-send-notification-1.0.0-SNAPSHOT.jar
    environment:
      APP_USER_LOGIN: ${self:custom.environment.${opt:stage, 'dev'}.DATABASE_CUSTOMER_MOBILE_TABLE}
      GCM_APPLICATION_NAME: ${self:custom.common.GlobeOneSNS.${opt:stage, 'dev'}.gcm-application-name}
      GCM_APPLICATION_ARN: ${self:custom.common.GlobeOneSNS.${opt:stage, 'dev'}.gcm-application-arn}
      APNS_APPLICATION_NAME: ${self:custom.common.GlobeOneSNS.${opt:stage, 'dev'}.apns-application-name}
      APNS_APPLICATION_ARN: ${self:custom.common.GlobeOneSNS.${opt:stage, 'dev'}.apns-application-arn}
      # APNS_SANBOX_APPLICATION_NAME: PushNotificationOneGlobeDev
      # APNS_SANBOX_APPLICATION_ARN: arn:aws:sns:ap-southeast-1:495831675559:app/APNS_SANDBOX/PushNotificationOneGlobeDev
      SNS_ENDPOINT: https://sns:ap-southeast-1.amazonaws.com
      SNS_REGION: ap-southeast-1
    events:
      - http:
          path: /mobile/v1/push-notification
          method: post
          cors: ${self:custom.security.cors}
          authorizer: ${self:custom.security.cognito.${opt:stage, 'dev'}}

  MobileActivateEmailVerification:
    handler: ph.com.globe.globeone.lambda.ActivateEmailVerificationHandler
    package:
      artifact: ${self:custom.common.relatedPath.sourceDirectory}mobile-app/activate-email-verification/target/globeone-es-mobile-app-activate-email-verification-1.0.0-SNAPSHOT.jar
    environment:
      SPARCO_ENDPOINT: ${self:custom.common.GlobeEndPoint.${opt:stage, 'dev'}.spaco}/Authentication.asmx
    events:
      - http:
          path: /mobile/v1/activate-email-verification
          method: post
          cors: ${self:custom.security.cors}

  MobileCreateAccount:
    handler: ph.com.globe.globeone.lambda.MobileCreateAccountHandler
    package:
      artifact: ${self:custom.common.relatedPath.sourceDirectory}mobile-app/create-account/target/globeone-es-mobile-app-create-account-1.0.0-SNAPSHOT.jar
    environment:
       SPARCO_ENDPOINT: ${self:custom.common.GlobeEndPoint.${opt:stage, 'dev'}.spaco}/Authentication.asmx
       SEND_EMAIL_FUNCTION_NAME: ${self:custom.common.functions.${opt:stage, 'dev'}.SendEmailNotification} 
    events:
      - http:
          path: /mobile/v1/account
          method: post
          cors: ${self:custom.security.cors}

  MobileResendEmailVerificationCode:
    handler: ph.com.globe.globeone.lambda.ResendEVCHandler
    package:
      artifact: ${self:custom.common.relatedPath.sourceDirectory}mobile-app/resend-email-verification-code/target/globeone-es-mobile-app-resend-email-verification-code-1.0.0-SNAPSHOT.jar
    environment:
       SPARCO_ENDPOINT: ${self:custom.common.GlobeEndPoint.${opt:stage, 'dev'}.spaco}/Authentication.asmx
       SEND_EMAIL_FUNCTION_NAME: ${self:custom.common.functions.${opt:stage, 'dev'}.SendEmailNotification} 
    events:
      - http:
          path: /mobile/v1/resend-email-verification-code
          method: post
          cors: ${self:custom.security.cors}              