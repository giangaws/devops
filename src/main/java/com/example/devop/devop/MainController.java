package com.example.devop.devop;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

	 // ?�n nh?n request GET
    @GetMapping("/") // N?u ng??i d�ng request t?i ??a ch? "/"
    public String index() {
        return "index"; // Tr? v? file index.html
    }

}
